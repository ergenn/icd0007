<?php
require_once 'lib/tpl.php';
require_once 'SqliteEntryDao.php';
include 'Entry.php';
$cmd = isset($_REQUEST['cmd'])?$_REQUEST['cmd']:"list_page";
$data = [];
$data['$title'] = 'Ergo rakendus';
$dao = new SqliteEntryDao();
if($cmd === "save"){
    if (strlen($_POST['firstName']) < 2 || strlen($_POST['firstName']) > 50) {
        $data['$errors']['firstName'] = "Eesnimi peab olema 2 kuni 50 märki!";
        $data['$firstNameClass'] = "error-border";
    }
    if (strlen($_POST['lastName']) < 2 || strlen($_POST['lastName']) > 50) {
        $data['$errors']['lastName'] = "Perekonnanimi peab olema 2 kuni 50 märki!";
        $data['$lastNameClass'] = "error-border";
    }
    $entry = (new Entry($_POST['id'], $_POST['firstName'], $_POST['lastName']))->phone($_POST['phone1'])->phone($_POST['phone2'])->phone($_POST['phone3']);
    if (isset($data['$errors'])) {
        $data['$entry'] = $entry;
        $data['$template'] = 'templates/add_page.html';
        $data['$title'] = 'Ergo rakendus | Lisa';
        $data['$add_selected'] = "selected";
        print render_template('templates/main.html', $data);
    } else {
        $dao->save($entry);
        header("Location: ?");
    }
} else {
    if ($cmd === "list_page") {
        $data['$template'] = 'templates/list_page.html';
        $data['$title'] = 'Ergo rakendus | Nimekiri';
        $data['$list_selected'] = "selected";
        $entries = $dao->findAll();
        $data['$entries'] = $entries;
    } elseif ($cmd === "add_page") {
        $data['$template'] = 'templates/add_page.html';
        $data['$title'] = 'Ergo rakendus | Lisa';
        $data['$add_selected'] = "selected";
    } elseif ($cmd === "edit_page") {
        $data['$template'] = 'templates/add_page.html';
        $data['$title'] = 'Ergo rakendus | Muuda';
        $data['$add_selected'] = "selected";
        $data['$entry'] = $dao->getEntryById($_GET['person_id']);
    }

    print render_template('templates/main.html', $data);
}