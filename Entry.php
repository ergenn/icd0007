<?php
/**
 * Created by PhpStorm.
 * User: Ergo
 * Date: 2018-03-20
 * Time: 13:17
 */

class Entry {
    public $id;
    public $firstName;
    public $lastName;
    public $phones = array();

    public function __construct($id, $firstName, $lastName) {
        $this->id = $id;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    public function addPhone($phone) {
        if (!empty($phone)) {
            $this->phones[] = $phone;
        }
    }

    public function phone($phone) {
        $this->addPhone($phone);
        return $this;
    }

    public function addPhones(array $phones) {
        $this->phones = array_merge($this->phones, $phones);
    }

    public function getPhones() {
        return implode(", ", $this->phones);
    }

    public function getEditHref() {
        return "?cmd=edit_page&person_id=" . $this->id;
    }
}