<?php
/**
 * Created by PhpStorm.
 * User: Ergo
 * Date: 2018-04-19
 * Time: 15:18
 */

class SqliteEntryDao {

    private $db;

    /**
     * SqliteEntryDao constructor.
     */
    public function __construct() {
        $this->db = new PDO("sqlite:data.sqlite");
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function save(Entry $entry) {
        if (is_numeric($entry->id)) {
            $this->edit($entry);
        } else {
            $this->insert($entry);
        }

    }

    public function insert(Entry $entry) {
        $stmt = $this->db->prepare("INSERT INTO entries (first_name, last_name) VALUES (:first_name, :last_name)");
        $stmt->bindValue(":first_name", $entry->firstName);
        $stmt->bindValue(":last_name", $entry->lastName);
        $stmt->execute();

        $id = $this->db->lastInsertId();

        foreach ($entry->phones as $phone) {
            $stmt = $this->db->prepare("INSERT INTO phonenumbers (entry_id, phone) VALUES (:entry_id, :phone)");
            $stmt->bindValue(":entry_id", $id);
            $stmt->bindValue(":phone", $phone);
            $stmt->execute();
        }
    }

    public function edit(Entry $entry) {
        $stmt = $this->db->prepare("UPDATE entries SET first_name=:first_name, last_name=:last_name WHERE id=:id");
        $stmt->bindValue(":first_name", $entry->firstName);
        $stmt->bindValue(":last_name", $entry->lastName);
        $stmt->bindValue(":id", $entry->id);
        $stmt->execute();

        $stmt = $this->db->prepare("DELETE FROM phonenumbers WHERE entry_id=:id");
        $stmt->bindValue(":id", $entry->id);
        $stmt->execute();

        $id = $entry->id;

        foreach ($entry->phones as $phone) {
            $stmt = $this->db->prepare("INSERT INTO phonenumbers (entry_id, phone) VALUES (:entry_id, :phone)");
            $stmt->bindValue(":entry_id", $id);
            $stmt->bindValue(":phone", $phone);
            $stmt->execute();
        }

    }

    public function getEntryById($id) {
        $stmt = $this->db->prepare("SELECT id, first_name, last_name, phone FROM entries LEFT JOIN phonenumbers ON id=entry_id WHERE id=:id");
        $stmt->bindValue(":id", $id);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $entries = array();

        foreach ($rows as $row) {
            if (array_key_exists($row['id'], $entries)) {
                $entries[$row['id']]->addPhone($row['phone']);
            } else {
                $entry = new Entry($row['id'], $row['first_name'], $row['last_name']);
                $entry->addPhone($row['phone']);
                $entries[$row['id']] = $entry;
            }
        }
        return $entries[array_keys($entries)[0]];
    }

    public function findAll() {
        $stmt = $this->db->prepare("SELECT id, first_name, last_name, phone FROM entries LEFT JOIN phonenumbers ON id=entry_id");
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $entries = array();

        foreach ($rows as $row) {
            if (array_key_exists($row['id'], $entries)) {
                $entries[$row['id']]->addPhone($row['phone']);
            } else {
                $entry = new Entry($row['id'], $row['first_name'], $row['last_name']);
                $entry->addPhone($row['phone']);
                $entries[$row['id']] = $entry;
            }
        }
        return $entries;

    }


}